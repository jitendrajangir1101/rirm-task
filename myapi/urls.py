from django.urls import path,include
from . import views

urlpatterns = [
    path('',views.home , name = 'home'),
    path('userRegistration/',views.userRegistration, name='userRegistration'),
    path('searchMember/',views.searchMember ,name='searchMember'),
    path('getImages/',views.getImages ,name='getImages'),
]
