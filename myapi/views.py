from django.shortcuts import render
from .models import userData
from .forms import userForm ,userSearchForm ,imageSearchForm
from django.contrib import messages
from django.http import HttpResponseRedirect
import requests
import matplotlib.pyplot as plt
from PIL import Image
from . import html_utils
# Create your views here.
def home(request):
    return render (request , 'myapi/home.html')
def userRegistration(request):
    form = userForm()
    if request.method == 'POST':
        form = userForm(request.POST or None)
        if form.is_valid():
            name = form.cleaned_data['name']
            URL = form.cleaned_data['URL']
            phoneNumber = form.cleaned_data['phoneNumber']
            userObject = userData(name=name,URL=URL ,phoneNumber=phoneNumber)
            userObject.save()
            messages.success(request, "you are registered with us !!!!")
            return HttpResponseRedirect('/userRegistration/')
            
    return render(request , 'myapi/userRegistration.html',{'form':form})

def searchMember(request):
    form = userSearchForm()
    if request.method == 'POST':
        form = userSearchForm(request.POST or None)
        name = request.POST.get('name')
        queryset =userData.objects.all().filter(name=name)

        return render(request, 'myapi/searchMember.html', {'queryset': queryset ,'form': form})
    return render(request, 'myapi/searchMember.html', {'form': form})

def getImages(request):
    form = imageSearchForm()
    if request.method == 'POST':
        form = imageSearchForm(request.POST or None)
        URL = request.POST.get('URL')
        print(URL)
        


        from PIL import Image
        import PIL.ImageOps  
        import requests
        import io

        try:
            r = requests.get(URL, stream=True)
            img = Image.open(io.BytesIO(r.content))
            return render_to_response('myapi/getImages.html', {
                'images_html': images_html}, context_instance=RequestContext(request))
            
        except:
            return render(request ,'myapi/getImages.html',{'form':form ,'except':"Image not found"})
    return render(request ,'myapi/getImages.html',{'form':form})
    images_html = [html_utils.image_to_html(image) for image in images]

    # ...

    