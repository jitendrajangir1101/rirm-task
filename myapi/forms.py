from django import forms
from .models import userData


#Form for adding user information  
class userForm(forms.ModelForm):
    
    class Meta:
        model = userData
        fields = '__all__'
        widgets = {
        'name' :forms.TextInput(attrs={'class':'form-control'}),
        'URL' :forms.TextInput(attrs={'class':'form-control'}),
        'phoneNumber' :forms.TextInput(attrs={'class':'form-control'}),
        }

class userSearchForm(forms.ModelForm):
    class Meta:
        model=userData
        fields = ('name',)
        widgets = {
            'name' :forms.TextInput(attrs={'class':'form-control'})
            }

class imageSearchForm(forms.ModelForm):
    class Meta:
        model=userData
        fields = ('URL',)
        widgets = {
            'URL' :forms.TextInput(attrs={'class':'form-control'})
            }